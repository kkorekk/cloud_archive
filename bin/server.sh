#!/usr/bin/env bash

docker-compose run \
    --service-ports \
    --rm \
    --entrypoint "/bin/bash -c" \
     webapp "python manage.py migrate && python manage.py runserver_plus 0:8000"
