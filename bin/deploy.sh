#!/usr/bin/env bash
eval $(docker-machine env cloud-archive)
docker-compose -f production.yml stop
docker-compose -f production.yml build
docker-compose -f production.yml up -d

