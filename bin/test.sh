#!/usr/bin/env bash

docker-compose run \
    --service-ports \
    --rm \
    --entrypoint "/bin/bash -c" \
    webapp \
    pytest -s tests/
