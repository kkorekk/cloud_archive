#!/usr/bin/env bash
docker-compose run \
    --service-ports \
    --rm \
    --entrypoint "/bin/bash -c" \
     webapp "pip install ipython && python manage.py migrate && python manage.py shell_plus --print-sql"
