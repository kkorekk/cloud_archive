import hashlib
import os


class Path:

    def __init__(self, user, path):
        path = path.strip()
        if path != '/' and path.endswith('/'):
            path = path[:-1]

        # Due to bug in angular-file-manager we have to fix path
        # that's mean we have to rename it from //path to /path
        if path.startswith('//'):
            path = path[1:]

        self.user = user
        self.path = path

    def __str__(self):
        return '<Path: {}>'.format(self.path)

    __repr__ = __str__

    @property
    def basename(self):
        return os.path.basename(self.path)

    @property
    def parent(self):
        return os.path.dirname(self.path)

    @property
    def ltree_parent(self):
        return self._hashed_path(self.parent)

    @property
    def ltree_path(self):
        return self._hashed_path(self.path)

    def _hashed_path(self, path):
        dirs = path.split('/')[1:]

        if not dirs:
            return ''

        if not dirs[0]:
            return self.user.md5_email

        return '{}.{}'.format(
            self.user.md5_email,
            '.'.join(
                self.hashed_string(dirname) for dirname in dirs
            )
        )

    @staticmethod
    def hashed_string(value):
        value = value.encode('utf-8')
        return hashlib.md5(value).hexdigest()
