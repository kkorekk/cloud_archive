from django.db.models import Lookup


class Match(Lookup):
    lookup_name = 'match'

    def __init__(self, lhs, rhs):
        super().__init__(lhs=lhs, rhs=rhs)

    def as_postgresql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)

        params = lhs_params + rhs_params

        return '%s ~ %s' % (lhs, rhs), params

    def as_sql(self, compiler, connection):
        raise NotImplemented(
            'This method is not available for given vendor {}'
            .format(connection.vendor)
        )


class Descendant(Lookup):
    lookup_name = 'descendant'

    def __init__(self, lhs, rhs):
        super().__init__(lhs=lhs, rhs=rhs)

    def as_postgresql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)

        params = lhs_params + rhs_params

        return '%s <@ %s' % (lhs, rhs), params

    def as_sql(self, compiler, connection):
        raise NotImplemented(
            'This method is not available for given vendor {}'
            .format(connection.vendor)
        )
