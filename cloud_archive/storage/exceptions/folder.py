"""This module contains all exception classes for Folder model."""


class BaseFolderException(Exception):
    """
    This is a base class for all exception related to Folder model.
    """


class CreateFolderException(BaseFolderException):
    """
    This is a base class for all exception related to creation new folders.
    """
