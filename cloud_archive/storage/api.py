from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .authentication import CsrfExemptSessionAuthentication
from .permissions import IsVerified
from .serializers import (
    CreateFolderSerializer,
    CopyFileSerializer,
    DeleteItemsSerializer,
    ListFolderSerializer,
    MoveItemsSerializer,
    RenameItemSerializer,
)


class BaseAPIView(GenericAPIView):
    authentication_classes = (CsrfExemptSessionAuthentication,)
    permission_classes = (IsVerified, )
    csrf_exempt = True

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return self.process_success()
        else:
            return self.process_error(error_message=serializer.errors)

    def process_error(
            self, error_message, status_code=status.HTTP_400_BAD_REQUEST
    ):
        return Response(
            status=status_code,
            data={
                'result': {
                    'success': False,
                    'error': self._get_flat_error(error=error_message),
                },
            }
        )

    @staticmethod
    def _get_flat_error(error):
        if isinstance(error, dict):
            output = set()
            for errors in error.values():
                output.update(errors)
            return ', '.join(output)
        return error

    @staticmethod
    def process_success(data=None, status_code=status.HTTP_200_OK):
        if data is None:
            data = {'success': True, 'error': None}

        return Response(status=status_code, data={'result': data})


class CreateFolderAPIView(BaseAPIView):
    serializer_class = CreateFolderSerializer


class ListFolderAPIView(BaseAPIView):
    serializer_class = ListFolderSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return self.process_success(serializer.data)
        else:
            return self.process_error(error_message=serializer.errors)


class RenameItemAPIView(BaseAPIView):
    serializer_class = RenameItemSerializer


class MoveItemsAPIView(BaseAPIView):
    serializer_class = MoveItemsSerializer


class CopyFileAPIView(BaseAPIView):
    serializer_class = CopyFileSerializer


class DeleteItemsAPIView(BaseAPIView):
    serializer_class = DeleteItemsSerializer
