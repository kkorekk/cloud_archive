from django.db import models

from .path import Path


class FolderManager(models.Manager):
    ROOT_FOLDER_NAME = 'root'

    def create_root_folder(self, user):
        """
        Create initial root folder for user.

        :param user: This must be an instance of cloud_user.users.User
        """
        return self.create(
            name=self.ROOT_FOLDER_NAME, owner=user, path=user.md5_email,
        )

    def create_by_path(self, path):
        return self.create(
            name=path.basename, owner=path.user, path=path.ltree_path,
        )

    def create_by_path_many(self, path_list):
        to_create = [
            self.model(
                name=path.basename,
                owner=path.user,
                path=path.ltree_path,
            ) for path in path_list
        ]
        return self.bulk_create(to_create)

    def create_by_path_str(self, user, path):
        path = Path(user=user, path=path)
        return self.create_by_path(path)

    def create_by_path_str_many(self, user, path_list):
        return self.create_by_path_many(
            [Path(user=user, path=path) for path in path_list]
        )
