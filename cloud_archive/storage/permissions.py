from rest_framework.permissions import IsAuthenticated


class IsVerified(IsAuthenticated):

    def has_permission(self, request, view):
        is_authenticated = super().has_permission(request, view)

        if is_authenticated:
            return request.user.is_verified
        else:
            return is_authenticated
