from django.conf.urls import url

from .api import (
    CopyFileAPIView,
    CreateFolderAPIView,
    DeleteItemsAPIView,
    ListFolderAPIView,
    MoveItemsAPIView,
    RenameItemAPIView,
)
from .views import DownloadFileView, FileManagerView, UploadFilesView


app_name = 'storage'

urlpatterns = [
    url('^file-manager/$', FileManagerView.as_view(), name='file-manager'),
    url(
        '^file-manager/upload/$',
        UploadFilesView.as_view(),
        name='file-upload',
    ),
    url(
        '^file-manager/download/$',
        DownloadFileView.as_view(),
        name='file-download',
    ),
    url(
        '^file-manager/folder/create/$',
        CreateFolderAPIView.as_view(),
        name='folder-create',
    ),
    url(
        '^file-manager/folder/list/$',
        ListFolderAPIView.as_view(),
        name='folder-list',
    ),
    url(
        '^file-manager/item/rename/$',
        RenameItemAPIView.as_view(),
        name='item-rename',
    ),
    url(
        '^file-manager/items/move/$',
        MoveItemsAPIView.as_view(),
        name='items-move',
    ),
    url(
        '^file-manager/items/delete/$',
        DeleteItemsAPIView.as_view(),
        name='items-delete',
    ),
    url(
        '^file-manager/file/copy/$',
        CopyFileAPIView.as_view(),
        name='file-copy',
    ),
]
