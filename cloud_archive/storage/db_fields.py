from django.db import models

from .db_lookups import Descendant, Match


class LTreeField(models.Field):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def db_type(self, connection):
        return 'ltree'


LTreeField.register_lookup(Descendant)
LTreeField.register_lookup(Match)
