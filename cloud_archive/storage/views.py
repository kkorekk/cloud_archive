from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import (
    HttpResponseBadRequest, HttpResponse, HttpResponseRedirect, JsonResponse,
)
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import TemplateView, View


from .models import File, Folder
from .path import Path


class FileManagerView(LoginRequiredMixin, TemplateView):
    template_name = 'file-manager/filemanager.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated() and not request.user.is_verified:
            return HttpResponseRedirect(
                reverse('users:verification-checkpoint'),
            )
        else:
            return super().dispatch(request, *args, **kwargs)


class UploadFilesView(LoginRequiredMixin, View):
    csrf_exempt = True

    def post(self, request, *args, **kwargs):
        if 'destination' not in request.POST:
            return JsonResponse(
                data={
                    'result': {
                        'success': True,
                        'error': 'missing destination in POST',
                    },
                },
            )

        self.create_files_from_request(request)

        return JsonResponse(
            data={'result': {'success': True, 'error': None}},
            status=200,
        )

    @staticmethod
    def create_files_from_request(request):
        folder = get_object_or_404(
            Folder,
            path=Path(
                user=request.user,
                path=request.POST['destination']
            ).ltree_path
        )

        return File.objects.bulk_create([
            File(
                folder=folder,
                storage_location=uploaded_file,
                name=uploaded_file.name,
                owner=request.user,
            )
            for uploaded_file in request.FILES.values()
        ])


class DownloadFileView(LoginRequiredMixin, View):
    download_proxy_template = '/download-proxy/{}'

    def get(self, request):
        path = request.GET.get('path')
        if path is None:
            return HttpResponseBadRequest()

        path = Path(user=request.user, path=path)
        file = get_object_or_404(
            File, folder__path=path.ltree_parent, name=path.basename,
        )

        if settings.USE_S3:
            response = HttpResponse()
            response['X-Accel-Redirect'] = self.download_proxy_template.format(
                file.storage_location.url.replace('https://', ''),
            )
        else:
            response = HttpResponse(file.storage_location.file)

        response['Content-Disposition'] = (
            'attachment; filename="{}"'.format(file.name)
        )

        return response
