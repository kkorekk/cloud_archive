from datetime import datetime
import hashlib

from django.conf import settings
from django.db import models, transaction
from django.db.models import F, Value

from .db_fields import LTreeField
from .db_functions import NLevelFunc, SubPathFunc
from .managers import FolderManager
from .path import Path


def generate_upload_to(instance, filename):
    encoded_filename = filename.encode('utf-8')
    encoded_now = datetime.now().isoformat().encode('utf-8')

    return '{}/{}'.format(
        instance.owner.md5_email,
        hashlib.md5(encoded_filename + encoded_now).hexdigest(),
    )


class File(models.Model):
    FILE_NAME_MAX_LENGTH = 255
    created = models.DateTimeField(auto_now_add=True, null=False)
    folder = models.ForeignKey(
        'storage.Folder',
        null=False,
        related_name='files',
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=FILE_NAME_MAX_LENGTH, null=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE,
    )
    storage_location = models.FileField(
        upload_to=generate_upload_to, null=False,
    )

    class Meta:
        unique_together = ('folder', 'name')

    def __str__(self):
        return self.name

    def rename(self, path):
        self.name = path.basename
        self.save(update_fields=['name'])

    def move(self, folder):
        self.folder = folder
        self.save(update_fields=['folder'])


class Folder(models.Model):
    FOLDER_NAME_MAX_LENGTH = 255
    created = models.DateTimeField(auto_now_add=True, null=False)
    name = models.CharField(max_length=FOLDER_NAME_MAX_LENGTH, null=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=False, on_delete=models.CASCADE
    )
    path = LTreeField(unique=True, null=False)

    objects = FolderManager()

    class Meta:
        unique_together = ('path', 'name')

    def __str__(self):
        return self.name

    @transaction.atomic
    def delete(self, *args, **kwargs):
        super().delete(*args, **kwargs)
        Folder.objects.filter(path__descendant=self.path).delete()

    def get_subfolders(self):
        return Folder.objects.filter(
            path__match='{}.*{{1}}'.format(self.path),
        )

    @transaction.atomic
    def rename(self, path):
        old_path = self.path
        self.name = path.basename
        self.path = path.ltree_path
        self.save(update_fields=['name', 'path'])

        folders = Folder.objects.filter(path__descendant=old_path)
        folders.update(
            path=path.ltree_path + SubPathFunc(
                F('path'), NLevelFunc(Value(path.ltree_path))
            )
        )

    def move(self, new_path):
        # We have to merge new path with the name of the old path
        # to correctly set ltree path
        new_merged_path = Path(
            user=new_path.user,
            path='{}/{}'.format(new_path.path, self.name),
        )
        old_path = self.path

        self.path = new_merged_path.ltree_path
        self.save(update_fields=['path'])

        folders = Folder.objects.filter(path__descendant=old_path)
        folders.update(
            path=new_merged_path.ltree_path + SubPathFunc(
                F('path'), NLevelFunc(Value(old_path))
            )
        )
