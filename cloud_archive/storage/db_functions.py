"""
This file contain django representation of ltree functions.
"""
from django.db.models import Func


class SubPathFunc(Func):
    """
    Representation of subpath function.
    """
    ADD = '||'
    function = 'subpath'


class NLevelFunc(Func):
    """
    Representation of nlevel function.
    """
    function = 'nlevel'
