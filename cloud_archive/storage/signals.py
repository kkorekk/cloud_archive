import logging

from django.contrib.auth import get_user_model
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

from .models import File, Folder


log = logging.getLogger(__name__)
User = get_user_model()


@receiver(post_save, sender=User)
def create_root_folder(**kwargs):
    created, instance = kwargs['created'], kwargs['instance']
    if created:
        Folder.objects.create_root_folder(user=instance)
        log.debug('created root folder for user %s', instance.email)


@receiver(post_delete, sender=File)
def file_post_delete_handler(**kwargs):
    file = kwargs['instance']

    storage, name = file.storage_location.storage, file.storage_location.name
    file_references = File.objects.filter(storage_location=name).exists()

    if not file_references:
        storage.delete(name)
