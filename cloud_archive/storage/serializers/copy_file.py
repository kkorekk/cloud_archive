from django.db import transaction
from rest_framework import serializers

from ..models import Folder, File
from .fields import PathField
from .mixins import UniqueParentFoldersMixin


class CopyFileSerializer(UniqueParentFoldersMixin, serializers.Serializer):
    items = serializers.ListField(
        child=PathField(),
        error_messages={
            'not_a_list': 'Expected a list of items',
        },
        min_length=1,
        required=True,
    )
    new_path = PathField(required=True)
    single_filename = serializers.CharField(required=False)

    default_error_messages = {
        'destination_not_exist': 'Destination folder does not exist',
        'item_exist': (
            'Invalid filename or already exists, specify another name'
        ),
        'more_than_one_file': (
            'Only one file in items is allowed when copying with filename',
        ),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._files = []
        self._destination_folder = None

    def validate_items(self, items):
        parent_folder = self.get_parent_folder(items)
        self._files = File.objects.filter(
            folder=parent_folder,
            owner=self.owner,
            name__in=[path.basename for path in items],
        )
        return items

    def validate_new_path(self, new_path):
        try:
            self._destination_folder = Folder.objects.get(
                owner=self.owner,
                path=new_path.ltree_path,
            )
        except Folder.DoesNotExist:
            self.fail('destination_not_exist')

        return new_path

    def validate_single_filename(self, single_filename):
        if single_filename:
            if len(self._files) != 1:
                self.fail('more_than_one_file')

        return single_filename

    def validate(self, attrs):
        items = attrs['items']
        any_file_exist = self._destination_folder.files.filter(
            folder=self._destination_folder,
            owner=self.owner,
            name__in=[path.basename for path in items],
        ).exists()
        if any_file_exist:
            self.fail('item_exist')

        return attrs

    @transaction.atomic
    def save(self, **kwargs):
        single_filename = self.validated_data.get('single_filename')

        if single_filename:
            File.objects.create(
                folder=self._destination_folder,
                name=single_filename,
                owner=self.owner,
                storage_location=self._files[0].storage_location,
            )
        else:
            File.objects.bulk_create(
                [
                    File(
                        folder=self._destination_folder,
                        name=file.name,
                        storage_location=file.storage_location,
                        owner=self.owner
                    ) for file in self._files
                ]
            )

    def create(self, validated_data):
        raise NotImplementedError(
            'create is not allowed for {}'.format(self.__class__.__name__),
        )

    def update(self, instance, validated_data):
        raise NotImplementedError(
            'update is not allowed for {}'.format(self.__class__.__name__),
        )
