from django.db import transaction
from rest_framework import serializers

from ..models import File, Folder
from .fields import PathField
from .mixins import OwnerMixin


class RenameItemSerializer(OwnerMixin, serializers.Serializer):
    item = PathField(required=True)
    new_item_path = PathField(required=True)

    default_error_messages = {
        'item_exist': (
            'Invalid filename or already exists, specify another name'
        ),
        'item_not_exist': 'Item does not exist',
        'parent_not_match': 'Parent folders does not match',
    }

    def validate_item(self, item):
        try:
            self.instance = File.objects.get(
                folder__path=item.ltree_parent,
                name=item.basename,
                owner=self.owner,
            )
        except File.DoesNotExist:
            try:
                self.instance = Folder.objects.get(path=item.ltree_path)
            except Folder.DoesNotExist:
                self.fail('item_not_exist')

        return item

    def validate_new_item_path(self, new_item_path):
        folder_exist = Folder.objects.filter(
            owner=self.owner,
            path=new_item_path.ltree_path,
        ).exists()
        if folder_exist:
            self.fail('item_exist')

        file_exist = File.objects.filter(
            folder__path=new_item_path.ltree_parent,
            name=new_item_path.basename,
            owner=self.owner,
        ).exists()
        if file_exist:
            self.fail('item_exist')

        return new_item_path

    def validate(self, attrs):
        item, new_item_path = attrs['item'], attrs['new_item_path']

        if item.ltree_parent != new_item_path.ltree_parent:
            self.fail('parent_not_match')

        return attrs

    def create(self, validated_data):
        raise NotImplementedError(
            'create is not allowed for {}'.format(self.__class__.__name__),
        )

    @transaction.atomic()
    def update(self, instance, validated_data):
        instance.rename(validated_data['new_item_path'])
        instance.save()
        return instance
