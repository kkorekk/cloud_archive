from rest_framework import serializers

from ..path import Path


class PathField(serializers.CharField):

    def to_representation(self, value):
        pass

    def to_internal_value(self, data):
        data = super().to_internal_value(data=data)
        return Path(user=self.context['request'].user, path=data)
