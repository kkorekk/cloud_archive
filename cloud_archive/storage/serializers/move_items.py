from django.db import transaction
from rest_framework import serializers

from ..models import File, Folder
from .fields import PathField
from .mixins import UniqueParentFoldersMixin


class MoveItemsSerializer(UniqueParentFoldersMixin, serializers.Serializer):
    items = serializers.ListField(
        child=PathField(),
        error_messages={
            'not_a_list': 'Expected a list of items',
        },
        min_length=1,
        required=True,
    )
    new_path = PathField(required=True)

    default_error_messages = {
        'destination_not_exist': 'Destination folder does not exist',
        'item_exist': (
            'Invalid filename or already exists, specify another name'
        ),
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._destination_folder = None
        self._files = []
        self._folders = []
        self._parent_folder = None

    def validate_items(self, items):
        self._parent_folder = self.get_parent_folder(items)
        self._files = File.objects.filter(
            folder=self._parent_folder,
            name__in=[path.basename for path in items],
            owner=self.owner,
        )
        self._folders = Folder.objects.filter(
            owner=self.owner,
            path__in=[path.ltree_path for path in items]
        )
        return items

    def validate_new_path(self, new_path):
        try:
            self._destination_folder = Folder.objects.get(
                owner=self.owner, path=new_path.ltree_path
            )
        except Folder.DoesNotExist:
            self.fail('destination_not_exist')
        return new_path

    def validate(self, attrs):
        items = attrs['items']

        any_folder_exist = self._destination_folder.get_subfolders().filter(
            owner=self.owner,
            name__in=[path.basename for path in items],
        ).exists()
        if any_folder_exist:
            self.fail('item_exist')

        any_file_exist = File.objects.filter(
            folder=self._destination_folder,
            owner=self.owner,
            name__in=[path.basename for path in items],
        ).exists()
        if any_file_exist:
            self.fail('item_exist')

        return attrs

    @transaction.atomic
    def save(self, **kwargs):
        for file in self._files:
            file.move(self._destination_folder)

        for folder in self._folders:
            folder.move(self.validated_data['new_path'])

    def create(self, validated_data):
        raise NotImplementedError(
            'create is not allowed for {}'.format(self.__class__.__name__),
        )

    def update(self, instance, validated_data):
        raise NotImplementedError(
            'update is not allowed for {}'.format(self.__class__.__name__),
        )
