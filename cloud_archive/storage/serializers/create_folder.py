from rest_framework import serializers

from ..models import Folder
from .fields import PathField
from .mixins import OwnerMixin


class CreateFolderSerializer(OwnerMixin, serializers.Serializer):
    new_path = PathField(required=True)

    default_error_messages = {
        'folder_name_too_long': (
            'Folder name too long (maximum length is {max_length})'
        ),
        'item_exist': (
            'Invalid filename or already exists, specify another name'
        ),
        'parent_not_exist': 'Parent folder does not exist',
    }

    def validate_new_path(self, new_path):
        if len(new_path.basename) > Folder.FOLDER_NAME_MAX_LENGTH:
            self.fail(
                'folder_name_too_long',
                max_length=Folder.FOLDER_NAME_MAX_LENGTH,
            )

        parent_exist = Folder.objects.filter(
            owner=self.owner,
            path=new_path.ltree_parent,
        ).exists()
        if not parent_exist:
            self.fail('parent_not_exist')

        folder_exist = Folder.objects.filter(
            owner=self.owner,
            path=new_path.ltree_path,
        )
        if folder_exist:
            self.fail('item_exist')

        return new_path

    def create(self, validated_data):
        new_path = validated_data['new_path']
        return Folder.objects.create(
            name=new_path.basename,
            owner=self.owner,
            path=new_path.ltree_path,
        )

    def update(self, instance, validated_data):
        raise NotImplementedError(
            'update is not allowed for {}'.format(self.__class__.__name__),
        )
