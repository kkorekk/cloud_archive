from django.db import transaction
from rest_framework import serializers

from ..models import File, Folder
from .fields import PathField
from .mixins import UniqueParentFoldersMixin


class DeleteItemsSerializer(UniqueParentFoldersMixin, serializers.Serializer):
    items = serializers.ListField(
        child=PathField(),
        error_messages={
            'not_a_list': 'Expected a list of items',
        },
        min_length=1,
        required=True,
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._files = []
        self._folders = []

    def validate_items(self, items):
        parent_folder = self.get_parent_folder(items)
        self._files = File.objects.filter(
            folder=parent_folder,
            name__in=[path.basename for path in items],
            owner=self.owner,
        )
        self._folders = Folder.objects.filter(
            owner=self.owner,
            path__in=[path.ltree_path for path in items],
        )
        return items

    @transaction.atomic
    def save(self, **kwargs):
        for file in self._files:
            file.delete()

        for folder in self._folders:
            folder.delete()

    def create(self, validated_data):
        raise NotImplementedError(
            'create is not allowed for {}'.format(self.__class__.__name__),
        )

    def update(self, instance, validated_data):
        raise NotImplementedError(
            'update is not allowed for {}'.format(self.__class__.__name__),
        )
