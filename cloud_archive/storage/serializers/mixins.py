from rest_framework import serializers

from ..models import Folder


class OwnerMixin(serializers.BaseSerializer):

    @property
    def owner(self):
        return self.context['request'].user


class UniqueParentFoldersMixin(OwnerMixin):
    default_error_messages = {
        'parent_folder_does_not_exist': 'Parent folder does not exist',
        'parent_folder_not_unique': (
            'Parent folder should be unique for all items in list'
        ),
    }

    def get_parent_folder(self, items):
        parent_folders = list({path.ltree_parent for path in items})
        if len(parent_folders) > 1:
            self.fail('parent_folder_not_unique')

        try:
            return Folder.objects.get(
                owner=self.owner,
                path=parent_folders[0]
            )
        except Folder.DoesNotExist:
            self.fail('parent_folder_does_not_exist')
