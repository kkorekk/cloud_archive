from rest_framework import serializers

from ..models import Folder
from .fields import PathField
from .mixins import OwnerMixin


class ListFolderSerializer(OwnerMixin, serializers.Serializer):
    path = PathField(required=True)

    default_error_messages = {
        'folder_not_exist': 'Folder does not exist'
    }

    def validate_path(self, path):
        try:
            folder = Folder.objects.prefetch_related('files').get(
                owner=self.owner,
                path=path.ltree_path,
            )
            self.instance = folder
        except Folder.DoesNotExist:
            self.fail('folder_not_exist')

        return path

    @property
    def data(self):
        return super(serializers.Serializer, self).data

    def to_representation(self, folder):
        files = folder.files.all()
        folders = folder.get_subfolders()
        return self.get_data(files=files, folders=folders)

    @staticmethod
    def get_data(files, folders):
        result = []
        result.extend(
            {
                'name': file_obj.name,
                'size': file_obj.storage_location.size,
                'date': file_obj.created.strftime('%Y-%m-%d %H:%M:%S'),
                'type': 'file',
            } for file_obj in files
        )
        result.extend(
            {
                'name': folder.name,
                'size': 0,
                'date': folder.created.strftime('%Y-%m-%d %H:%M:%S'),
                'type': 'dir',
            } for folder in folders
        )
        return result

    def create(self, validated_data):
        raise NotImplementedError(
            'create is not allowed for {}'.format(self.__class__.__name__),
        )

    def update(self, instance, validated_data):
        raise NotImplementedError(
            'update is not allowed for {}'.format(self.__class__.__name__),
        )
