from django.conf.urls import url
from django.contrib.auth.views import (
    LoginView,
    LogoutView,
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView,
)
from django.urls import reverse_lazy

from .forms import PasswordResetAsyncForm
from .views import (
    DeleteProfileView,
    RegisterView,
    UpdateProfileView,
    VerificationCheckpointView,
    VerifyUserAccountView,
)


app_name = 'users'

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^profile/$', UpdateProfileView.as_view(), name='edit-profile'),
    url(
        r'^profile/delete/$',
        DeleteProfileView.as_view(),
        name='delete-profile',
    ),
    url(
        r'^password/reset/$',
        PasswordResetView.as_view(
            form_class=PasswordResetAsyncForm,
            success_url=reverse_lazy('users:password-reset-done'),
            email_template_name='mail/password_reset_email.mjml',
            subject_template_name='mail/password_reset_subject.txt',
            html_email_template_name='mail/password_reset_email.mjml',
        ),
        name='password-reset',
    ),
    url(
        r'^password/reset/done/$',
        PasswordResetDoneView.as_view(),
        name='password-reset-done',
    ),
    url(
        r'^password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        PasswordResetConfirmView.as_view(
            success_url=reverse_lazy('users:password-reset-complete'),
        ),
        name='password-reset-complete',
    ),
    url(
        r'^password/complete/$',
        PasswordResetCompleteView.as_view(),
        name='password-reset-complete',
    ),
    url(
        r'^verify/checkpoint/$',
        VerificationCheckpointView.as_view(),
        name='verification-checkpoint',
    ),
    url(
        r'^verify/(?P<token>[:\-\d\w]+)/$',
        VerifyUserAccountView.as_view(),
        name='verify-account',
    ),
]
