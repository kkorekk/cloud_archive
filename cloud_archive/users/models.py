import hashlib

from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core import signing
from django.utils.crypto import get_random_string


class User(AbstractBaseUser):
    USERNAME_FIELD = 'email'

    email = models.EmailField(null=False, blank=False, unique=True)
    first_name = models.CharField(max_length=255, null=False, blank=True)
    is_active = models.BooleanField(default=True)
    last_name = models.CharField(max_length=255, null=False, blank=True)
    date_joined = models.DateTimeField(
        auto_now_add=True, null=False, blank=False,
    )
    is_verified = models.BooleanField(default=False)
    verification_token = models.CharField(max_length=15)

    objects = BaseUserManager()

    @property
    def md5_email(self):
        return hashlib.md5(self.email.encode('utf-8')).hexdigest()

    def get_full_name(self):
        full_name = '{} {}'.format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def get_signed_verification_token(self):
        return signing.dumps(self.verification_token)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.verification_token = get_random_string(length=15)
        return super().save(*args, **kwargs)

    def verify_user(self):
        self.is_verified = True
        self.verification_token = ''
        self.save(update_fields=('is_verified', 'verification_token'))
