from urllib.parse import ParseResult, urlunparse

from django.contrib.auth.forms import UserCreationForm, PasswordResetForm
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse

from .models import User
from .tasks import send_mail


class RegisterForm(UserCreationForm):
    from_email = None
    email_template_name = 'mail/verification_email.txt'
    html_email_template_name = 'mail/verification_email.mjml'
    subject_template_name = 'mail/verification_email_subject.txt'

    class Meta:
        fields = ('email', 'first_name', 'last_name',)
        model = User

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        user = super().save(commit=commit)
        self.send_verification_mail(user)

        return user

    def send_verification_mail(self, user):
        current_site = get_current_site(self.request)

        protocol = 'https' if self.request.is_secure() else 'http'
        domain = current_site.domain

        parsed_url = ParseResult(
            scheme=protocol,
            netloc=domain,
            path=reverse(
                'users:verify-account',
                kwargs={'token': user.get_signed_verification_token()},
            ),
            params='',
            query='',
            fragment='',
        )
        verification_email_url = urlunparse(parsed_url)

        send_mail.delay(
            subject_template_name=self.subject_template_name,
            email_template_name=self.email_template_name,
            context={
                'verification_email_url': verification_email_url,
                'first_name': user.first_name,
            },
            from_email=self.from_email,
            to_email=user.email,
            html_email_template_name=self.html_email_template_name,
        )


class PasswordResetAsyncForm(PasswordResetForm):

    def send_mail(
            self, subject_template_name, email_template_name,
            context, from_email, to_email, html_email_template_name=None
    ):
        parsed_url = ParseResult(
            scheme=context['protocol'],
            netloc=context['domain'],
            path=reverse(
                'users:password-reset-complete',
                kwargs={'uidb64': context['uid'], 'token': context['token']}
            ),
            params='',
            query='',
            fragment='',
        )
        password_reset_url = urlunparse(parsed_url)
        send_mail.delay(
            subject_template_name=subject_template_name,
            email_template_name=email_template_name,
            context={'password_reset_url': password_reset_url},
            from_email=from_email,
            to_email=to_email,
            html_email_template_name=html_email_template_name,
        )
