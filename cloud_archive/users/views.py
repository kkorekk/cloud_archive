from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login
from django.core import signing
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views.generic.base import View
from django.views.generic import DeleteView, FormView, UpdateView

from .forms import RegisterForm
from .models import User


class DeleteProfileView(LoginRequiredMixin, DeleteView):
    template_name = 'delete-profile.html'

    def delete(self, request, *args, **kwargs):
        response = super().delete(request, *args, **kwargs)
        return response

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('users:login')


class RegisterView(FormView):
    form_class = RegisterForm
    template_name = 'registration/register.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return super().form_valid(form=form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        return reverse('storage:file-manager')


class UpdateProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'edit-profile.html'
    model = User
    fields = ('first_name', 'last_name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.password_form = None
        self.object = None

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['folders_num'] = self.object.folder_set.all().count() - 1
        context['files_num'] = self.object.file_set.all().count()
        context['email'] = self.object.email
        context['joined'] = self.object.date_joined.strftime('%x')
        context['password_form'] = self.password_form
        return context

    def get_form_kwargs(self):
        if 'password-form' in self.request.POST:
            return {
                'initial': self.get_initial(),
                'prefix': self.get_prefix(),
                'instance': self.object
            }

        return super().get_form_kwargs()

    def get_object(self, queryset=None):
        return self.request.user

    def get_success_url(self):
        return reverse('users:edit-profile')

    def get(self, request, *args, **kwargs):
        self.password_form = PasswordChangeForm(user=self.get_object())
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'password-form' in request.POST:
            self.password_form = PasswordChangeForm(
                user=self.object, data=request.POST,
            )
            if self.password_form.is_valid():
                self.password_form.save()
                return HttpResponseRedirect(self.get_success_url())
            else:
                return self.render_to_response(self.get_context_data())
        else:
            return super().post(request, *args, **kwargs)


class VerificationCheckpointView(LoginRequiredMixin, View):

    def get(self, request):
        return TemplateResponse(request, 'verification/checkpoint.html')

    def post(self, request):
        RegisterForm(request=request).send_verification_mail(request.user)
        return TemplateResponse(
            request, 'verification/resend_email_confirm.html',
        )


class VerifyUserAccountView(View):

    def get(self, request, token):
        try:
            verification_token = signing.loads(token)
        except signing.BadSignature:
            return TemplateResponse(request, 'verification/bad_signature.html')

        try:
            user = User.objects.get(verification_token=verification_token)
        except User.DoesNotExist:
            if request.user.is_authenticated():
                return HttpResponseRedirect(reverse('storage:file-manager'))
            return HttpResponseRedirect(reverse('users:login'))
        else:
            user.verify_user()

        return TemplateResponse(request, 'verification/confirm.html')
