import environ
import os

from django.urls import reverse_lazy

env = environ.Env()
env.ENVIRON = os.environ.copy()


# Common settings section
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = env.str('SECRET_KEY')

DEBUG = env.bool('DEBUG')

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS')

WSGI_APPLICATION = 'cloud_archive.wsgi.application'

ROOT_URLCONF = 'cloud_archive.urls'


# Installed apps section
INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third parties
    'django_extensions',
    'storages',
    'mjml',

    # Local applications
    'storage',
    'users',
]


# Middleware section
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # TODO: re-enable csrf
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


# Templates section
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, '..', 'templates'), ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Databases section
DATABASES = {
    'default': env.db(),
}


# Auth section
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': (
            'django.contrib.auth.password_validation.'
            'UserAttributeSimilarityValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.MinimumLengthValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.CommonPasswordValidator'
        ),
    },
    {
        'NAME': (
            'django.contrib.auth.password_validation.NumericPasswordValidator'
        ),
    },
]

AUTH_USER_MODEL = 'users.User'

LOGIN_URL = reverse_lazy('users:login')

LOGIN_REDIRECT_URL = reverse_lazy('storage:file-manager')

LOGOUT_REDIRECT_URL = LOGIN_URL


# Internationalization section
TIME_ZONE = 'UTC'

USE_I18N = False

USE_L10N = False

USE_TZ = True


# Static files section
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, '..', 'static'),
    os.path.join(BASE_DIR, '..', 'static', 'bower_components')
]

STATIC_ROOT = os.path.join(BASE_DIR, '..', '..', 'static')


# Media file section
MEDIA_ROOT = os.path.join(BASE_DIR, '..', '..', 'media')

MEDIA_URL = '/media/'


# Amazon S3 section
USE_S3 = env.bool('USE_S3')

if USE_S3:
    AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = env.str('AWS_STORAGE_BUCKET_NAME')
    AWS_S3_REGION_NAME = env.str('AWS_S3_REGION_NAME')

    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'


# Email section
EMAIL_HOST = 'smtp.sparkpostmail.com'

EMAIL_PORT = 587

EMAIL_HOST_USER = 'SMTP_Injection'

EMAIL_HOST_PASSWORD = env.str('EMAIL_HOST_PASSWORD')

EMAIL_USE_TLS = True

DEFAULT_FROM_EMAIL = env.str('DEFAULT_FROM_EMAIL')


# Django rest framework section
REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
}


# Celery section
CELERY_BROKER_URL = env.str('CELERY_BROKER_URL')


# django-mjml section
MJML_BACKEND_MODE = 'tcpserver'
MJML_TCPSERVERS = [
    ('mjml', 28101),
]
