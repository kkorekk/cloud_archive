from .base import *


DEFAULT_FILE_STORAGE = 'inmemorystorage.InMemoryStorage'


EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'


CELERY_TASK_ALWAYS_EAGER = True
CELERY_TASK_EAGER_PROPAGATES = True
