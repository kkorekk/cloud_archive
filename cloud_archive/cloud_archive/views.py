from django.http import HttpResponseRedirect
from django.urls import reverse


def root_view(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('storage:file-manager'))
    else:
        return HttpResponseRedirect(reverse('users:login'))
