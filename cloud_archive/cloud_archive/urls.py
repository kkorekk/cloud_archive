from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static

from .views import root_view

urlpatterns = [
    url(r'^$', root_view, name='root-view'),
    url(r'^user/', include('users.urls')),
    url(r'^storage/', include('storage.urls'))
] + (
    static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)
