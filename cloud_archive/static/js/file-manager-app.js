angular.module('FileManagerApp').config(['fileManagerConfigProvider', function (config) {
    let defaults = config.$get();

    config.set({
        appName: 'Cloud Archive',
        basePath: '/',
        copyUrl: 'file/copy/',
        createFolderUrl: 'folder/create/',
        downloadFileUrl: 'download/',
        listUrl: 'folder/list/',
        moveUrl: 'items/move/',
        removeUrl: 'items/delete/',
        renameUrl: 'item/rename/',
        uploadUrl: 'upload/',

        enablePermissionsRecursive: false,
        defaultLang: 'en',
        hidePermissions: true,
        allowedActions: angular.extend(defaults.allowedActions, {
            changePermissions: false,
            compress: false,
            edit: false,
            extract: false,
            downloadMultiple: false,
            pickFiles: false,
            pickFolders: false,
            preview: true,
        }),
    });
}]);
