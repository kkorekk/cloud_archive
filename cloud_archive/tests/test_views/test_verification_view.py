from django.core import mail, signing
from django.urls import reverse
import pytest


pytestmark = pytest.mark.django_db()


def test_successful_verification(anonymous_client, user):
    signed_token = user.get_signed_verification_token()
    response = anonymous_client.get(
        reverse('users:verify-account', kwargs={'token': signed_token})
    )

    assert response.status_code == 200
    assert response.template_name == 'verification/confirm.html'

    user.refresh_from_db()
    assert user.is_verified
    assert user.verification_token is ''


def test_bad_token_signature(anonymous_client, user):
    user.is_verified = False
    user.save()
    signed_token = signing.dumps(user.verification_token, key='EXAMPLE_KEY')

    response = anonymous_client.get(
        reverse('users:verify-account', kwargs={'token': signed_token})
    )

    assert response.status_code == 200
    assert response.template_name == 'verification/bad_signature.html'

    user.refresh_from_db()
    assert not user.is_verified


def test_redirect_when_sending_verification_twice(anonymous_client, user):
    signed_token = user.get_signed_verification_token()

    response = anonymous_client.get(
        reverse('users:verify-account', kwargs={'token': signed_token})
    )

    assert response.status_code == 200

    response = anonymous_client.get(
        reverse('users:verify-account', kwargs={'token': signed_token})
    )
    assert response.status_code == 302
    assert response.url == reverse('users:login')

    anonymous_client.force_login(user)
    response = anonymous_client.get(
        reverse('users:verify-account', kwargs={'token': signed_token})
    )
    assert response.status_code == 302
    assert response.url == reverse('storage:file-manager')


def test_resend_verification_mail(logged_client):
    logged_client.user.is_verified = False
    logged_client.user.save()

    response = logged_client.post(reverse('users:verification-checkpoint'))
    assert response.status_code == 200

    verification_mail = mail.outbox[0]
    assert verification_mail.to == [logged_client.user.email]
    assert verification_mail.subject == 'Verify your account on Cloud Archive'
