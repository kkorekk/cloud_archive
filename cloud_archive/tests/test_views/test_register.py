from django.core import mail
from django.urls import reverse
import pytest


pytestmark = pytest.mark.django_db()


def test_signin_page_available(client):
    response = client.get(reverse('users:login'))

    assert response.status_code == 200


def test_register_page_available(client):
    response = client.get(reverse('users:register'))

    assert response.status_code == 200


def test_send_verification_email(client):
    response = client.post(
        reverse('users:register'),
        data={
            'email': 'example_user@example.com',
            'first_name': 'fname',
            'last_name': 'lname',
            'password1': 'TestPassword12#',
            'password2': 'TestPassword12#',
        },
        follow=True,
    )

    assert response.status_code == 200
    assert mail.outbox

    verification_mail = mail.outbox[0]
    assert verification_mail.to == ['example_user@example.com']
    assert verification_mail.subject == 'Verify your account on Cloud Archive'
