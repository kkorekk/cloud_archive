"""
Tests for DownloadFileView
"""
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework import status

from storage.models import File, Folder


def test_download_only_for_logged_user(anonymous_client):
    """
    DownloadFileView should return 403 if user is anonymous.
    """
    url = reverse('storage:file-download')
    response = anonymous_client.get(url)

    assert response.status_code == status.HTTP_302_FOUND
    assert response.url == '{}?next={}'.format(reverse('users:login'), url)


def test_download_not_existing_file(logged_client):
    """
    DownloadFileView should return 404 if path not exists.
    """
    url = '{}?path={}'.format(
        reverse('storage:file-download'),
        '/parent/file_0.txt',
    )
    response = logged_client.get(url)

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_download_file(logged_client):
    """
    DownloadFileView should successfully download file.
    """
    user = logged_client.user
    folder = Folder.objects.create_by_path_str(user=user, path='/parent')
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    File.objects.create(
        folder=folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )

    url = '{}?path={}'.format(
        reverse('storage:file-download'),
        '/parent/file_0.txt',
    )
    response = logged_client.get(url)

    assert response.status_code == status.HTTP_200_OK
