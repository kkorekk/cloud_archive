"""Tests for root_view."""
from django.urls import reverse
from rest_framework import status


def test_redirect_to_login_view(anonymous_client):
    """Test that root view redirects to the login page."""
    response = anonymous_client.get(reverse('root-view'))

    assert response.status_code == status.HTTP_302_FOUND
    assert response.url == reverse('users:login')


def test_redirect_to_file_manager_view(logged_client):
    """Test that root view redirects to the file-manager page."""
    response = logged_client.get(reverse('root-view'))

    assert response.status_code == status.HTTP_302_FOUND
    assert response.url == reverse('storage:file-manager')
