from django.urls import reverse


def test_file_manager_available(logged_client):
    url = reverse('storage:file-manager')
    response = logged_client.get(url)

    assert response.status_code == 200


def test_file_manager_available_only_for_logged_users(anonymous_client):
    url = reverse('storage:file-manager')
    response = anonymous_client.get(url)

    assert response.status_code == 302
    assert response.url == '{}?next={}'.format(reverse('users:login'), url)


def test_file_manager_redirect_to_verification_checkpoint(logged_client):
    url = reverse('storage:file-manager')
    logged_client.user.is_verified = False
    logged_client.user.save()
    response = logged_client.get(url)

    assert response.status_code == 302
    assert response.url == reverse('users:verification-checkpoint')
