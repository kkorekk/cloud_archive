"""
Tests for UploadFilesView
"""
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework import status

from storage.models import File, Folder
from storage.path import Path


def test_upload_view_available_only_for_logged_users(anonymous_client):
    """
    Upload-file view should be available only for logged in users.
    """
    url = reverse('storage:file-upload')
    response = anonymous_client.get(url)

    assert response.status_code == status.HTTP_302_FOUND
    assert response.url == '{}?next={}'.format(reverse('users:login'), url)


def test_upload_to_non_existing_path(logged_client):
    """Upload-file view should return 404 if destination doesn't exist."""
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )

    url = reverse('storage:file-upload')
    response = logged_client.post(
        url,
        {
            'destination': '/parent/folder_a',
            'file-0': plain_file_0,
        },
    )

    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_upload_two_files(logged_client):
    """
    Upload-file view should save in db uploaded files data.
    """
    user = logged_client.user
    Folder.objects.create_by_path_str(user=user, path='/parent')
    parent_folder = Folder.objects.create_by_path_str(
        user=user, path='/parent/folder_a',
    )
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )

    url = reverse('storage:file-upload')
    response = logged_client.post(
        url,
        {
            'destination': '/parent/folder_a',
            'file-0': plain_file_0,
            'file-1': plain_file_1,
        },
    )
    assert response.status_code == status.HTTP_200_OK

    files = File.objects.all()
    assert files.count() == 2

    file_0 = files.get(name='file_0.txt')
    file_1 = files.get(name='file_1.txt')

    assert file_0.owner == user
    assert file_0.folder == parent_folder
    assert file_1.owner == user
    assert file_1.folder == parent_folder
