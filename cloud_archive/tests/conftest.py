import pytest


@pytest.fixture
def user(django_user_model):
    user_instance = django_user_model.objects.create(
        email='test_user@example.com',
        first_name='First',
        last_name='Last',
        is_verified=True,
    )
    user_instance.set_password('password')
    user_instance.save()

    return user_instance


@pytest.fixture
def anonymous_client(client):
    return client


@pytest.fixture
def logged_client(client, user):
    client.force_login(user)
    client.user = user
    return client
