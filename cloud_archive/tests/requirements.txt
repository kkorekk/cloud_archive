pytest
pytest-django
pytest-cov==2.4.0
freezegun
pylama
ambition-inmemorystorage==1.4.0
