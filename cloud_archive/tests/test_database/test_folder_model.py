"""
Tests for Folder model and FolderManager manager.
"""
import hashlib
import pytest

from storage.models import Folder
from storage.managers import FolderManager
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_create_root_folder(user):
    """
    Test that root folder is correctly created.
    """
    # Remove created by a signal models
    Folder.objects.all().delete()
    folder = Folder.objects.create_root_folder(user)

    assert folder.name == FolderManager.ROOT_FOLDER_NAME
    assert folder.path == hashlib.md5(user.email.encode('utf-8')).hexdigest()
    assert folder.owner == user


def test_create_root_folder_when_creating_user(django_user_model):
    """
    Test that root folder is created automatically when creating an user.
    """
    user = django_user_model.objects.create(
        email='test_user@example.com',
        first_name='First',
        last_name='Last',
    )
    folder = Folder.objects.get(owner=user)

    assert folder.path == hashlib.md5(user.email.encode('utf-8')).hexdigest()


def test_rename_folder(user):
    """
    Test that folder can be successfully renamed.
    """
    query_path = Path(user=user, path='/new_parent')
    folder = Folder.objects.create_by_path_str(user=user, path='/parent')
    Folder.objects.create_by_path_str_many(
        user,
        path_list=[
            '/parent/folder_a',
            '/parent/folder_b',
            '/parent/folder_b/sub_folder',
        ],
    )
    folder.rename(query_path)
    folder.refresh_from_db()

    assert folder.name == query_path.basename
    assert folder.path == query_path.ltree_path

    folders = Folder.objects.filter(path__descendant=query_path.ltree_path)
    assert len(folders) == 4
