import pytest

from storage.models import Folder
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_descendant_lookup(user):
    query_path = Path(user=user, path='/parent')
    Folder.objects.create_by_path_str_many(
        user,
        path_list=[
            '/parent',
            '/parent/folder_a',
            '/parent/folder_b',
        ]
    )
    query_set = Folder.objects.filter(path__descendant=query_path.ltree_path)

    assert len(query_set) == 3
