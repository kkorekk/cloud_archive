from storage.models import File, Folder
from storage.serializers import MoveItemsSerializer


def test_move_items_with_valid_data(user, fake_post):
    # Prepare fake data
    source = Folder.objects.create_by_path_str(user, '/source')
    destination = Folder.objects.create_by_path_str(user, '/destination')
    Folder.objects.create_by_path_str_many(
        user=user, path_list=['/source/folder-a', '/source/folder-b'],
    )
    File.objects.create(
        folder=source, name='f1.txt', owner=user, storage_location='f1',
    )
    File.objects.create(
        folder=source, name='f2.txt', owner=user, storage_location='f2',
    )

    # Prepare serializer instance
    data = {
        'items': [
            '/source/folder-a',
            '/source/folder-b',
            '/source/f1.txt',
            '/source/f2.txt',
        ],
        'new_path': '/destination',
    }
    serializer = MoveItemsSerializer(
        data=data, context={'request': fake_post},
    )

    # Validate serializer
    assert serializer.is_valid()
    serializer.save()

    # Check that all items where moved to the destination folder
    assert len(destination.get_subfolders()) == 2
    assert len(destination.files.all()) == 2
