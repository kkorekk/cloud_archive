import pytest

from storage.models import Folder
from storage.path import Path
from storage.serializers import CreateFolderSerializer


pytestmark = pytest.mark.django_db()


def _get_serializer_instance(data, request):
    return CreateFolderSerializer(data=data, context={'request': request})


def test_create_folder_with_valid_data(user, fake_post):
    parent_path = Path(user=user, path='/parent')
    new_folder_path = Path(user=user, path='/parent/new-folder')
    Folder.objects.create_by_path(parent_path)
    data = dict(new_path=new_folder_path.path)

    serializer = _get_serializer_instance(data, fake_post)
    assert serializer.is_valid()
    serializer.save()

    assert Folder.objects.filter(
        owner=user, name='new-folder', path=new_folder_path.ltree_path
    ).exists()


def test_create_folder_name_too_long(user, fake_post):
    folder_path = Path(user=user, path='/{}'.format('A' * 256))
    data = dict(new_path=folder_path.path)

    serializer = _get_serializer_instance(data, fake_post)

    assert not serializer.is_valid()
    assert serializer.errors == {
        'new_path': ['Folder name too long (maximum length is 255)'],
    }

    folder_path = Path(user=user, path='/{}'.format('A' * 255))
    data = dict(new_path=folder_path.path)

    serializer = _get_serializer_instance(data, fake_post)

    assert serializer.is_valid()


def test_create_folder_parent_not_exists(user, fake_post):
    new_folder_path = Path(user=user, path='/parent/new-folder')
    data = dict(new_path=new_folder_path.path)

    serializer = _get_serializer_instance(data, fake_post)

    assert not serializer.is_valid()
    assert serializer.errors == {'new_path': ['Parent folder does not exist']}

