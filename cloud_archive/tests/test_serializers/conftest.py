import pytest


@pytest.fixture
def fake_post(rf, user):
    request = rf.post('/fake-path')
    request.user = user
    return request
