"""Tests for ListFolderAPIView."""
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from freezegun import freeze_time
import pytest
from rest_framework import status

from storage.models import File, Folder


pytestmark = pytest.mark.django_db()


def test_list_folder_available_only_for_logged_user(anonymous_client):
    """ListFolderAPIView should be visible only for logged users."""
    data = {'path': '/new_folder'}
    response = anonymous_client.post(
        reverse('storage:folder-list'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_list_folder_return_sub_folders(logged_client):
    """ListFolderAPIView should return 400 is path is not in request data."""
    data = {}
    response = logged_client.post(
        reverse('storage:folder-list'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'This field is required.',
        },
    }


def test_list_folder_return_400_when_path_not_exists(logged_client):
    data = {'path': '/not-existing-folder'}
    response = logged_client.post(
        reverse('storage:folder-list'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'Folder does not exist',
        }
    }


def test_list_folder_return_subfolders(logged_client):
    """ListFolderAPIView should return correct subfolders."""
    user = logged_client.user
    with freeze_time('2012-01-14 12:00:01'):
        Folder.objects.create_by_path_str_many(
            user=user,
            path_list=[
                '/parent',
                '/parent/folder_a',
                '/parent/folder_b',
                '/parent/folder_b/folder_c',

            ]
        )

    data = {'path': '/parent'}
    response = logged_client.post(
        reverse('storage:folder-list'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK

    assert len(response.json()['result']) == 2

    assert {
        'name': 'folder_a',
        'size': 0,
        'date': '2012-01-14 12:00:01',
        'type': 'dir',
    } in response.json()['result']

    assert {
        'name': 'folder_b',
        'size': 0,
        'date': '2012-01-14 12:00:01',
        'type': 'dir',
    } in response.json()['result']


def test_list_folder_contain_files(logged_client):
    """ListFolderAPIView should return correct files."""
    user = logged_client.user
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )

    with freeze_time('2012-01-14 12:00:01'):
        folder = Folder.objects.create_by_path_str(
            user=user, path='/parent',
        )
        sub_folder = Folder.objects.create_by_path_str(
            user=user, path='/parent/folder_a',
        )
        File.objects.create(
            folder=folder,
            name=plain_file_0.name,
            owner=user,
            storage_location=plain_file_0,
        )
        File.objects.create(
            folder=folder,
            name=plain_file_1.name,
            owner=user,
            storage_location=plain_file_1,
        )
        File.objects.create(
            folder=sub_folder,
            name=plain_file_1.name,
            owner=user,
            storage_location=plain_file_1,
        )

    data = {'path': '/parent'}
    response = logged_client.post(
        reverse('storage:folder-list'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK

    assert len(response.json()['result']) == 3

    assert {
       'name': 'folder_a',
       'size': 0,
       'date': '2012-01-14 12:00:01',
       'type': 'dir',
    } in response.json()['result']

    assert {
        'name': plain_file_0.name,
        'size': plain_file_0.size,
        'date': '2012-01-14 12:00:01',
        'type': 'file',
    } in response.json()['result']

    assert {
        'name': plain_file_1.name,
        'size': plain_file_1.size,
        'date': '2012-01-14 12:00:01',
        'type': 'file',
    } in response.json()['result']
