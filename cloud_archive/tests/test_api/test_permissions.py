from django.urls import reverse
import pytest


@pytest.mark.parametrize(
    'url', [
        'storage:folder-create',
        'storage:folder-list',
        'storage:item-rename',
        'storage:items-move',
        'storage:items-delete',
        'storage:file-copy',
    ]
)
def test_api_available_for_verified_user(logged_client, url):
    logged_client.user.is_verified = False
    logged_client.user.save()

    response = logged_client.post(reverse(url))
    assert response.status_code == 403
