"""Tests for DeleteResourceAPIView."""
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
import pytest
from rest_framework import status

from storage.models import File, Folder
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_delete_resource_available_only_for_logged_user(anonymous_client):
    """DeleteResourceAPIView should be visible only for logged users."""
    data = {'items': ['/folder-a']}
    response = anonymous_client.post(
        reverse('storage:items-delete'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_missing_key_in_request(logged_client):
    """
    DeleteResourceAPIView should return 400 if items not in request data.
    """
    data = {}

    response = logged_client.post(
        reverse('storage:items-delete'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False, 'error': 'This field is required.',
        }
    }


def test_items_not_a_list(logged_client):
    """
    DeleteResourceAPIView should return 400 if items is not a list.
    """
    data = {
        'items': {},
    }

    response = logged_client.post(
        reverse('storage:items-delete'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False, 'error': 'Expected a list of items',
        }
    }


def test_delete_file(logged_client):
    """
    DeleteResourceAPIView should delete file.
    """
    user = logged_client.user
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )
    parent_folder_path = Path(user=user, path='/parent')
    parent_folder = Folder.objects.create_by_path(parent_folder_path)

    File.objects.create(
        folder=parent_folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )
    File.objects.create(
        folder=parent_folder,
        name=plain_file_1.name,
        owner=user,
        storage_location=plain_file_1,
    )

    data = {'items': ['/parent/file_0.txt', '/parent/file_1.txt']}

    response = logged_client.post(
        reverse('storage:items-delete'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_200_OK
    assert not File.objects.filter(
        name=plain_file_0.name, folder__path=parent_folder_path.ltree_path,
    ).exists()
    assert not File.objects.filter(
        name=plain_file_1.name, folder__path=parent_folder_path.ltree_path,
    ).exists()


def test_delete_folder(logged_client, user):
    """
    DeleteResourceAPIView should delete folder and subfolders.
    """
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )
    root_path = Path(user=user, path='/')
    folder_a_path = Path(user=user, path='/folder_a')
    folder_b_path = Path(user=user, path='/folder_a/folder_b')
    folder_c_path = Path(user=user, path='/folder_a/folder_c')
    folder_d_path = Path(user=user, path='/folder_d')

    root_folder = Folder.objects.get(path=root_path.ltree_path)
    folder_b = Folder.objects.create_by_path(folder_b_path)
    Folder.objects.create_by_path_many(
        path_list=[folder_a_path, folder_c_path, folder_d_path]
    )

    File.objects.create(
        folder=root_folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )
    File.objects.create(
        folder=folder_b,
        name=plain_file_1.name,
        owner=user,
        storage_location=plain_file_1,
    )
    data = {'items': ['/folder_a']}

    response = logged_client.post(
        reverse('storage:items-delete'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_200_OK

    assert not Folder.objects.filter(path=folder_a_path.ltree_path).exists()
    assert not Folder.objects.filter(path=folder_b_path.ltree_path).exists()
    assert not Folder.objects.filter(path=folder_c_path.ltree_path).exists()
    assert Folder.objects.filter(path=folder_d_path.ltree_path).exists()

    assert File.objects.filter(
        name=plain_file_0.name, folder__path=root_path.ltree_path,
    ).exists()
    assert not File.objects.filter(
        name=plain_file_1.name, folder__path=folder_b_path.ltree_path,
    ).exists()
