"""Tests for MoveResourceAPIView."""
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
import pytest
from rest_framework import status

from storage.models import File, Folder
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_move_resource_available_only_for_logged_users(anonymous_client):
    """MoveResourceAPIView should be available only for logged users."""
    data = {
        'items': ['/public_html/libs', '/public_html/config.py'],
        'newPath': '/public_html/includes',
    }
    response = anonymous_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_missing_keys_in_request_data(logged_client):
    """
    MoveResourceAPIView should return 400 if keys are missing in request.
    """
    data = [
        {}
    ]

    for d in data:
        response = logged_client.post(
            reverse('storage:items-move'),
            content_type='application/json',
            data=json.dumps(d),
        )

        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.json() == {
            'result': {
                'success': False, 'error': 'This field is required.',
            }
        }


def test_items_is_not_a_list(logged_client):
    """
    MoveResourceAPIView should return 400 if items is not a list.
    """
    Folder.objects.create_by_path_str(logged_client.user, '/new-path')
    data = {
        'items': '/some-path',
        'newPath': '/new-path',
    }
    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'Expected a list of items',
        }
    }


def test_new_path_not_exist(logged_client):
    """
    MoveResourceAPIView should return 404 if newPath not exist.
    """
    user = logged_client.user
    Folder.objects.create_by_path_str(user=user, path='/some-path')

    data = {
        'items': ['/some-path'],
        'newPath': '/new-path',
    }

    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False, 'error': 'Destination folder does not exist',
        }
    }


def test_move_when_file_exist_in_destination(logged_client):
    # Prepare fake data
    source_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/source'
    )
    File.objects.create(
        folder=source_folder,
        name='file_0.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0.txt', b'file_content', content_type='text/plain',
        )
    )
    File.objects.create(
        folder=source_folder,
        name='file_1.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_1.txt', b'file_content', content_type='text/plain',
        )
    )
    destination_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination',
    )
    destination_file = File.objects.create(
        folder=destination_folder,
        name='file_0.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0.txt', b'file_content', content_type='text/plain',
        )
    )

    # Perform request
    data = {
        'items': ['/source/file_0.txt', '/source/file_1.txt'],
        'newPath': '/destination',
    }
    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }
    assert list(destination_folder.files.all()) == [destination_file]


def test_move_when_folder_exist_in_destination(logged_client):
    # Prepare fake data
    Folder.objects.create_by_path_str_many(
        user=logged_client.user,
        path_list=['/source', '/source/folder-a', '/source/folder-b']
    )
    destination_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination'
    )
    destination_subfolder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination/folder-a'
    )

    # Perform request
    data = {
        'items': ['/source/folder-a', '/source/folder-b'],
        'newPath': '/destination',
    }
    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }
    assert list(destination_folder.get_subfolders()) == [destination_subfolder]


def test_move_file_when_folder_exist_in_destination(logged_client):
    # Prepare fake data
    source_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/source'
    )
    File.objects.create(
        folder=source_folder,
        name='file_0',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0', b'file_content', content_type='text/plain',
        )
    )
    destination_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination'
    )
    Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination/file_0'
    )

    # Perform request
    data = {'items': ['/source/file_0'], 'newPath': '/destination'}
    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }
    assert list(destination_folder.files.all()) == []


def test_move_folder_when_file_exist_in_destination(logged_client):
    # Prepare fake data
    Folder.objects.create_by_path_str_many(
        user=logged_client.user,
        path_list=['/source', '/source/file_0'],
    )
    destination_folder = Folder.objects.create_by_path_str(
        user=logged_client.user, path='/destination'
    )
    File.objects.create(
        folder=destination_folder,
        name='file_0',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0', b'file_content', content_type='text/plain',
        )
    )

    # Perform request
    data = {'items': ['/source/file_0'], 'newPath': '/destination'}
    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }
    assert list(destination_folder.get_subfolders()) == []


def test_move_folder_and_files(logged_client):
    """
    MoveResourceAPIView should successfully move folders.
    """
    user = logged_client.user
    parent_folder = Folder.objects.create_by_path_str(
        user=user, path='/parent',
    )
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    File.objects.create(
        folder=parent_folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )
    Folder.objects.create_by_path_str_many(
        user=user,
        path_list=[
            '/parent/folder_a/',
            '/parent/folder_a/folder_b',
            '/parent/folder_c',
            '/target',
            '/target/target_folder'
        ]
    )

    data = {
        'items': [
            '/parent/folder_a', '/parent/folder_c', '/parent/file_0.txt',
        ],
        'newPath': '/target/target_folder',
    }

    response = logged_client.post(
        reverse('storage:items-move'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_200_OK
    expected_paths = [
        '/target/target_folder/folder_a',
        '/target/target_folder/folder_a/folder_b',
        '/target/target_folder/folder_c',
    ]

    for expected_path in expected_paths:
        p = Path(user=user, path=expected_path)
        folder = Folder.objects.get(
            path=p.ltree_path
        )
        assert folder.name == p.basename

    file = File.objects.get(
        folder__path=Path(user=user, path='/target/target_folder').ltree_path,
        name=plain_file_0.name,
    )
    assert file
