"""Tests for CopyFileAPIView."""
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
import pytest
from rest_framework import status

from storage.models import File, Folder
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_copy_file_available_only_for_logged_user(anonymous_client):
    """CopyFileAPIView should be visible only for logged users."""
    data = {'items': ['/folder-a/test.py'], 'newPath': '/folder-b'}
    response = anonymous_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_missing_keys_in_request_data(logged_client):
    """
    CopyFileAPIView should return 400 if keys are missing in request.
    """
    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps({}),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False, 'error': 'This field is required.',
        }
    }


def test_items_is_not_a_list(logged_client):
    """
    CopyFileAPIView should return 400 if items is not a list.
    """
    Folder.objects.create_by_path_str(
        user=logged_client.user, path='/folder-b',
    )
    data = {'items': '/folder-a/test.py', 'newPath': '/folder-b'}
    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False,
            'error': 'Expected a list of items',
        }
    }


def test_new_path_not_exist(logged_client):
    """
    CopyFileAPIView should return 404 if newPath not exist.
    """
    Folder.objects.create_by_path_str(logged_client.user, '/parent/folder_b')
    data = {
        'items': ['/parent/folder_a/file_0.txt'],
        'newPath': '/parent/folder_b/',
    }

    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {'error': 'Parent folder does not exist', 'success': False},
    }


def test_copy_when_file_in_destination_folder_exist(logged_client):
    # Prepare fake data
    source_folder = Folder.objects.create_by_path_str(
        logged_client.user, '/source',
    )
    destination_folder = Folder.objects.create_by_path_str(
        logged_client.user, '/destination',
    )
    File.objects.create(
        folder=source_folder,
        name='file_0.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0.txt', b'content', content_type='text/plain'
        )
    )
    File.objects.create(
        folder=source_folder,
        name='file_1.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_1.txt', b'content', content_type='text/plain'
        )
    )
    destination_file = File.objects.create(
        folder=destination_folder,
        name='file_0.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0.txt', b'content', content_type='text/plain'
        )
    )

    # Perform request
    data = {
        'items': ['/source/file_0.txt', '/source/file_1.txt'],
        'newPath': '/destination',
    }
    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'error': (
                'Invalid filename or already exists, specify another name'
            ),
            'success': False,
        }
    }
    assert list(destination_folder.files.all()) == [destination_file]


def test_copy_single_file(logged_client):
    """
    CopyFileAPIView should successfully copy single file.
    """
    user = logged_client.user
    Folder.objects.create_by_path_str(user=user, path='/parent')
    folder_a = Path(user=user, path='/parent/folder_a')
    folder_b = Path(user=user, path='/parent/folder_b')

    folder = Folder.objects.create_by_path(folder_a)
    Folder.objects.create_by_path(folder_b)

    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    File.objects.create(
        folder=folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )

    data = {
        'items': ['/parent/folder_a/file_0.txt'],
        'newPath': '/parent/folder_b/',
        'singleFilename': 'file_0_copy.txt',
    }

    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_200_OK

    source_file = File.objects.get(folder__path=folder_a.ltree_path)
    assert source_file.name == 'file_0.txt'

    file_copy = File.objects.get(folder__path=folder_b.ltree_path)
    assert file_copy.name == 'file_0_copy.txt'


def test_copy_files(logged_client):
    """
    CopyFileAPIView should successfully copy one than more file.
    """
    # Prepare fake data
    user = logged_client.user
    source_folder_path = Path(user=user, path='/source')
    source_folder = Folder.objects.create_by_path(source_folder_path)
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )
    plain_file_2 = SimpleUploadedFile(
        'file_2.txt', b'file_content', content_type='text/plain',
    )
    files = (plain_file_0, plain_file_1, plain_file_2)
    copied_files = files[:2]

    for plain_file in files:
        File.objects.create(
            folder=source_folder,
            name=plain_file.name,
            owner=user,
            storage_location=plain_file,
        )

    destination_folder_path = Path(user=user, path='/destination')
    destination_folder = Folder.objects.create_by_path(destination_folder_path)

    # Perform request
    data = {
        'items': [
            '/source/file_0.txt',
            '/source/file_1.txt',
        ],
        'newPath': '/destination',
    }
    response = logged_client.post(
        reverse('storage:file-copy'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was successful
    assert response.status_code == status.HTTP_200_OK
    assert destination_folder.files.all().count() == 2

    for plain_file in copied_files:
        source_file = File.objects.get(
            folder__path=source_folder_path.ltree_path,
            name=plain_file.name,
            owner=user,
        )
        assert source_file

        file_copy = File.objects.get(
            folder__path=destination_folder_path.ltree_path,
            name=plain_file.name,
            owner=user,
        )
        assert file_copy
        assert file_copy.storage_location == source_file.storage_location
