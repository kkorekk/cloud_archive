"""Tests for RenameResourceAPIView."""
import json

from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
import pytest
from rest_framework import status

from storage.models import File, Folder
from storage.path import Path


pytestmark = pytest.mark.django_db()


def test_rename_resource_available_only_for_logged_users(anonymous_client):
    """RenameResourceAPIView should be available only for logged users."""
    data = {
        'item': '/public_html/test.py',
        'newItemPath': '/public_html/test2.py',
    }
    response = anonymous_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_missing_keys_in_request_data(logged_client):
    """
    RenameResourceAPIView should return 400 if keys are missing in request.
    """
    data = {}
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'This field is required.',
        }
    }


def test_item_parent_path_not_match(logged_client):
    """
    RenameResourceAPIView should return 400 if parent path are not equal.
    """
    user = logged_client.user
    Folder.objects.create_by_path_str(user=user, path='/parent')
    Folder.objects.create_by_path_str(user=user, path='/parent/folder_a')
    data = {
        'item': '/parent/folder_a',
        'newItemPath': '/parent-test/folder_b',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'Parent folders does not match',
        }
    }


def test_resource_not_exists(logged_client):
    """
    RenameResourceAPIView should return 404 if resource not exist.
    """
    data = {
        'item': '/public_html/folder_a',
        'newItemPath': '/public_html/folder_b',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.data == {
        'result': {
            'success': False,
            'error': 'Item does not exist',
        }
    }


def test_rename_file_to_existing_file(logged_client):
    # Prepare fake data
    parent_folder = Folder.objects.create_by_path_str(
        user=logged_client.user,
        path='/parent'
    )
    File.objects.create(
        folder=parent_folder,
        name='file_0.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_0.txt', b'file_content', content_type='text/plain',
        )
    )
    File.objects.create(
        folder=parent_folder,
        name='file_1.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_1.txt', b'file_content', content_type='text/plain',
        )
    )

    # Perform request
    data = {
        'item': '/parent/file_0.txt',
        'newItemPath': '/parent/file_1.txt',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }


def test_rename_folder_to_existing_folder(logged_client):
    # Prepare fake data
    Folder.objects.create_by_path_str_many(
        user=logged_client.user, path_list=[
            '/parent',
            '/parent/folder-a',
            '/parent/folder-b',
        ],
    )

    # Perform request
    data = {
        'item': '/parent/folder-a',
        'newItemPath': '/parent/folder-b',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }


def test_rename_file_to_existing_folder(logged_client):
    # Prepare fake data
    parent_folder = Folder.objects.create_by_path_str(
        user=logged_client.user,
        path='/parent',
    )
    Folder.objects.create_by_path_str(
        user=logged_client.user,
        path='/parent/folder-a'
    )
    File.objects.create(
        folder=parent_folder,
        name='file_1.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_1.txt', b'file_content', content_type='text/plain',
        )
    )

    # Perform request
    data = {
        'item': '/parent/file_1.txt',
        'newItemPath': '/parent/folder-a',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }


def test_rename_folder_to_existing_file(logged_client):
    # Prepare fake data
    parent_folder = Folder.objects.create_by_path_str(
        user=logged_client.user,
        path='/parent',
    )
    Folder.objects.create_by_path_str(
        user=logged_client.user,
        path='/parent/folder-a'
    )
    File.objects.create(
        folder=parent_folder,
        name='file_1.txt',
        owner=logged_client.user,
        storage_location=SimpleUploadedFile(
            'file_1.txt', b'file_content', content_type='text/plain',
        )
    )

    # Perform request
    data = {
        'item': '/parent/folder-a',
        'newItemPath': '/parent/file_1.txt',
    }
    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )

    # Assert that operation was not successful
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            )
        }
    }


def test_file_rename(logged_client):
    """
    RenameResourceAPIView should successfully rename file.
    """
    user = logged_client.user
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )

    Folder.objects.create_by_path_str(user=user, path='/parent')
    folder = Folder.objects.create_by_path_str(
        user=user, path='/parent/folder_a',
    )
    File.objects.create(
        folder=folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )
    File.objects.create(
        folder=folder,
        name=plain_file_1.name,
        owner=user,
        storage_location=plain_file_1,
    )

    data = {
        'item': '/parent/folder_a/{}'.format(plain_file_0.name),
        'newItemPath': '/parent/folder_a/new_file_name.txt',
    }

    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data == {'result': {'success': True, 'error': None}}

    renamed_file = File.objects.get(
        folder__path=Path(
            path='/parent/folder_a', user=user,
        ).ltree_path,
        name='new_file_name.txt',
    )
    assert renamed_file


def test_folder_rename(logged_client):
    """
    RenameResourceAPIView should successfully rename file.
    """
    user = logged_client.user
    plain_file_0 = SimpleUploadedFile(
        'file_0.txt', b'file_content', content_type='text/plain',
    )
    plain_file_1 = SimpleUploadedFile(
        'file_1.txt', b'file_content', content_type='text/plain',
    )

    parent_folder = Folder.objects.create_by_path_str(
        user=user, path='/parent',
    )
    folder = Folder.objects.create_by_path_str(
        user=user, path='/parent/folder_a',
    )
    Folder.objects.create_by_path_str(user=user, path='/parent/folder_b')
    File.objects.create(
        folder=folder,
        name=plain_file_0.name,
        owner=user,
        storage_location=plain_file_0,
    )
    File.objects.create(
        folder=folder,
        name=plain_file_1.name,
        owner=user,
        storage_location=plain_file_1,
    )

    data = {
        'item': '/parent',
        'newItemPath': '/parent_renamed',
    }

    response = logged_client.post(
        reverse('storage:item-rename'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.data == {'result': {'success': True, 'error': None}}

    parent_folder.refresh_from_db()

    renamed_path = Path(user=user, path='/parent_renamed')
    assert parent_folder.name == 'parent_renamed'
    assert parent_folder.path == renamed_path.ltree_path

    folders = Folder.objects.filter(path__descendant=renamed_path.ltree_path)
    assert len(folders) == 3

    renamed_sub_folder = Path(user=user, path='/parent_renamed/folder_a')
    assert File.objects.get(
        name=plain_file_0.name, folder__path=renamed_sub_folder.ltree_path,
    )
    assert File.objects.get(
        name=plain_file_1.name, folder__path=renamed_sub_folder.ltree_path,
    )
