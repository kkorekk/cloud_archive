"""Tests for CreateFolderAPIView."""
import json

from django.urls import reverse
from rest_framework import status

from storage.models import Folder


def test_create_folder_available_only_for_logged_user(anonymous_client):
    """CreateFolderAPIView should be visible only for logged users."""
    data = {'newPath': '/new_folder'}
    response = anonymous_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_403_FORBIDDEN


def test_missing_new_path_in_request(logged_client):
    """CreateFolderAPIView should return 400 error is newPath is missing."""
    data = {}
    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {'success': False, 'error': 'This field is required.'}
    }


def test_folder_name_longer_than_255_chars(logged_client):
    """
    CreateFolderAPIView should return 400 error if folder
    name is longer than 255 chars.
    """
    data = {'newPath': '/{}'.format('a' * 256)}

    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'Folder name too long (maximum length is 255)',
        },
    }


def test_parent_folder_not_exist(logged_client):
    """
    CreateFolderAPIView should return 400 error if parent folder not exists.
    """
    data = {'newPath': '/parent-folder/child-folder'}
    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': 'Parent folder does not exist'
        },
    }


def test_create_existing_folder(logged_client):
    """
    CreateFolderAPIView should return 400 error if folder exists.
    """
    data = {'newPath': '/new-folder'}

    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK

    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {
        'result': {
            'success': False,
            'error': (
                'Invalid filename or already exists, specify another name'
            ),
        }
    }


def test_create_new_folder_in_root(logged_client):
    """CreateFolderAPIView should create a new folder inside users's root."""
    user = logged_client.user
    data = {'newPath': '/new_folder'}
    response = logged_client.post(
        reverse('storage:folder-create'),
        content_type='application/json',
        data=json.dumps(data),
    )
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'result': {'success': True, 'error': None}}

    folder = Folder.objects.get(name='new_folder')
    assert folder.owner == user
