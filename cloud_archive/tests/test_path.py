"""Tests for Path object."""
from storage.path import Path


def test_path_root(user):
    path = ' / '
    path_instance = Path(path=path, user=user)
    assert path_instance.basename == ''
    assert path_instance.ltree_parent == user.md5_email
    assert path_instance.ltree_path == user.md5_email


def test_path_root_sub_folder(user):
    path = ' /test-folder/ '
    path_instance = Path(path=path, user=user)

    assert path_instance.basename == 'test-folder'
    assert path_instance.ltree_parent == user.md5_email
    assert path_instance.ltree_path == '{}.{}'.format(
        user.md5_email, Path.hashed_string('test-folder'),
    )


def test_path_sub_folder(user):
    path = '/test-parent/test-child'
    path_instance = Path(path=path, user=user)

    assert path_instance.basename == 'test-child'
    assert path_instance.ltree_parent == '{}.{}'.format(
        user.md5_email,
        Path.hashed_string('test-parent'),
    )
    assert path_instance.ltree_path == '{}.{}.{}'.format(
        user.md5_email,
        Path.hashed_string('test-parent'),
        Path.hashed_string('test-child'),
    )


def test_path_sub_folder_ends_with_slash(user):
    path = '/test-parent/test-child/'
    path_instance = Path(path=path, user=user)

    assert path_instance.basename == 'test-child'
    assert path_instance.ltree_parent == '{}.{}'.format(
        user.md5_email,
        Path.hashed_string('test-parent'),
    )
    assert path_instance.ltree_path == '{}.{}.{}'.format(
        user.md5_email,
        Path.hashed_string('test-parent'),
        Path.hashed_string('test-child'),
    )
