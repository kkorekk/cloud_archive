#!/usr/bin/env bash

python manage.py migrate
python manage.py collectstatic --noinput

gunicorn cloud_archive.wsgi:application \
    --name cloud_archive \
    --bind 0.0.0.0:8000 \
    --workers 3 \
    "$@"
