#!/usr/bin/env bash

envsubst '$NGINX_STATIC_EXPIRES $NGINX_SERVER_NAME $NGINX_WEBAPP_HOST $NGINX_RESOLVER $SSL_CERTIFICATE $SSL_CERTIFICATE_KEY' \
< /srv/conf/cloud-archive.template > /etc/nginx/conf.d/cloud-archive.conf && nginx -g 'daemon off;'
